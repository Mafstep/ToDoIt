package ir.mehranbehnam.todoit.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedDate;

/**
 * Encapsulates Project parameters.
 *
 * @author Mehran Behnam
 */
@Entity
@Table(name = "projects")
public class Project {

  /**
   * Project Unique identity.
   */
  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Long id;
  /**
   * Represents project name.
   */
  private String name;
  /**
   * A short description for project.
   */
  private String description;
  /**
   * project creation time.
   */
  @CreatedDate
  private LocalDateTime createdAt;
  /**
   * Represents project completion.
   */
  private Boolean isComplete;
  /**
   * project creation time.
   */
  private LocalDateTime completedAt;
  /**
   * Relation between child projects and their parent
   */

  private Long parentId;
  /**
   * Relation between parent project and its children.
   */
  @OneToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "projects", joinColumns = @JoinColumn(name = "parentId", updatable = false), inverseJoinColumns = @JoinColumn(name = "id"))
  private List<Project> children;
  /**
   * Project order when its displayed in list.
   */
  private Integer priority;
  /**
   * Project Owner
   */
  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public Boolean isComplete() {
    return isComplete;
  }

  public void setComplete(Boolean complete) {
    isComplete = complete;
  }

  public LocalDateTime getCompletedAt() {
    return completedAt;
  }

  public void setCompletedAt(LocalDateTime completedAt) {
    this.completedAt = completedAt;
  }

  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public List<Project> getChildren() {
    return children;
  }

  public void setChildren(List<Project> children) {
    this.children = children;
  }

  public Integer getPriority() {
    return priority;
  }

  public void setPriority(Integer priority) {
    this.priority = priority;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}