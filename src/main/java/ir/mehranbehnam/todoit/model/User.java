package ir.mehranbehnam.todoit.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Encapsulates User important parameters.
 *
 * @author Mehran Behnam
 */
@Entity
@Table(name = "users")
public class User {

  /**
   * User Unique identity.
   */
  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Long id;
  /**
   * user Unique email.
   */
  @Column(unique = true)
  private String email;
  /**
   * user JWt access token.
   */
  @Column(unique = true)
  private String token;
  /**
   * user JWt access refresh token.
   */
  @Column(unique = true)
  private String refreshToken;
  /**
   * email verification code.
   */
  private String verificationCode;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  public void setRefreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
  }

  public String getVerificationCode() {
    return verificationCode;
  }

  public void setVerificationCode(String verificationCode) {
    this.verificationCode = verificationCode;
  }
}
