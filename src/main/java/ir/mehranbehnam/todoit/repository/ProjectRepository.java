package ir.mehranbehnam.todoit.repository;

import ir.mehranbehnam.todoit.model.Project;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {


}
