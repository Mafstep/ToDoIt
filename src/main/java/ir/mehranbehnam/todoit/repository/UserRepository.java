package ir.mehranbehnam.todoit.repository;

import ir.mehranbehnam.todoit.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {


}
