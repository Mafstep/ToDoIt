package ir.mehranbehnam.todoit.service;

import ir.mehranbehnam.todoit.service.mail.Email;

public interface MailService {

  void send(Email email);
}
