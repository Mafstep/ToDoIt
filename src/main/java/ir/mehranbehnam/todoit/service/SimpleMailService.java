package ir.mehranbehnam.todoit.service;

import ir.mehranbehnam.todoit.service.mail.Email;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class SimpleMailService implements MailService {


  @Value("todoit.mail.from")
  private String from;
  /**
   * Provides mail service to send mails.
   */
  private final JavaMailSender mailSender;
  /**
   * Provides TemplateEngine service.
   */
  private final TemplateEngine templateEngine;


  protected SimpleMailService(TemplateEngine templateEngine,
      JavaMailSender mailSender) {
    this.templateEngine = templateEngine;
    this.mailSender = mailSender;
  }

  public void send(Email email) {
    MimeMessagePreparator preparator = mimeMessage -> {
      MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);

      helper.setFrom(from);
      helper.setTo(email.getRecipient());
      helper.setSubject(email.getSubject());

      String content = getTemplate(email);
      helper.setText(content, true);
    };

    mailSender.send(preparator);
  }

  private String getTemplate(Email email) {
    Context context = new Context();
    email.getData().forEach(context::setVariable);
    return templateEngine.process(email.getTemplateName(), context);
  }
}
