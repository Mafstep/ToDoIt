package ir.mehranbehnam.todoit.service.mail;

import java.util.Map;

public abstract class Email {

  private String recipient;
  private String subject;
  private Map<String, Object> data;

  Email(
      String recipient, String subject, Map<String, Object> data
  ) {
    this.recipient = recipient;
    this.subject = subject;
    this.data = data;
  }

  public String getRecipient() {
    return this.recipient;
  }

  public String getSubject() {
    return this.subject;
  }

  public Map<String, Object> getData() {
    return this.data;
  }

  public abstract String getTemplateName();


}
