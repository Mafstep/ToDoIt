package ir.mehranbehnam.todoit.service.mail;

import java.util.Map;

public class VerificationEmail extends Email {

  public VerificationEmail(
      String recipient, String subject, Map<String, Object> data
  ) {
    super(recipient, subject, data);
  }

  @Override
  public String getTemplateName() {
    return "Verification";
  }
}
