package ir.mehranbehnam.todoit;

import static java.util.Collections.singletonList;

import org.testcontainers.containers.MySQLContainer;

/**
 * Responsible for providing test containers for test purposes.
 *
 * @author Mehran Behnam
 */
public class TestContainers {

    /**
     * Provide MySQL container for our tests.
     *
     * @see <a href="https://www.testcontainers.org/test_framework_integration/junit_5/">TestContainers</a>
     */
    public static MySQLContainer mysqlContainer() {
        MySQLContainer mySQLContainer = new MySQLContainer();
        mySQLContainer.withDatabaseName("todoit");
        mySQLContainer.setPortBindings(singletonList("3306:3306"));

        return mySQLContainer;
    }
}
