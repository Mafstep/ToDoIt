package ir.mehranbehnam.todoit.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import ir.mehranbehnam.todoit.TestContainers;
import ir.mehranbehnam.todoit.repository.ProjectRepository;
import ir.mehranbehnam.todoit.repository.UserRepository;
import java.util.Optional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests to test the entity mappings for {@link Project} entity.
 *
 * @author Mehran Behnam
 */
@DataJpaTest
@DirtiesContext
@Testcontainers
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = NONE)
class ProjectIT {

  /**
   * Spins up a MySQL instance before all tests and tears it down after it.
   */
  @Container
  private static final MySQLContainer MYSQL_CONTAINER = TestContainers.mysqlContainer();

  /**
   * Providers data access operations over {@link Project} entity.
   */
  @Autowired
  private ProjectRepository projectRepository;

  /**
   * Providers data access operations over {@link User} entity.
   */
  @Autowired
  private UserRepository userRepository;

  @BeforeEach
  void cleanDatabaseAfterEach() {
    projectRepository.deleteAll();
    userRepository.deleteAll();
  }

  @AfterAll
  static void stopMysql() {
    MYSQL_CONTAINER.stop();
  }

  @Test
  @DisplayName("We should be able persist in database as expected.")
  void Save_WeShouldBeAblePersistInDatabaseAsExpected() {
    User user = new User();
    user.setEmail("emxamole@host.com");
    user.setToken("token");
    user.setRefreshToken("refresh toj=ken");
    user.setVerificationCode("123-321");

    userRepository.save(user);

    Project project = new Project();
    project.setName("Project Name");
    project.setDescription("Project Description");
    project.setComplete(false);
    project.setPriority(1);
    project.setUser(user);

    projectRepository.save(project);

    Optional<Project> retrieved = projectRepository.findById(project.getId());

    assertThat(retrieved.isPresent()).isTrue();
    assertThat(retrieved.get().getId()).isEqualTo(1L);
    assertThat(retrieved.get().getName()).isEqualTo("Project Name");
    assertThat(retrieved.get().getDescription()).isEqualTo("Project Description");
    assertThat(retrieved.get().isComplete()).isEqualTo(false);
    assertThat(retrieved.get().getPriority()).isEqualTo(1);
    assertThat(retrieved.get().getUser()).isEqualTo(user);

  }

  /******************************************************************************************************************
   * Auxiliary Stuff
   *****************************************************************************************************************/

  @TestConfiguration
  @ComponentScan(basePackageClasses = {
      ProjectRepository.class,
      UserRepository.class
  })
  static class ProjectITConfig {

  }
}