package ir.mehranbehnam.todoit.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import ir.mehranbehnam.todoit.TestContainers;
import ir.mehranbehnam.todoit.repository.UserRepository;
import java.util.Optional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * Integration tests to test the entity mappings for {@link User} entity.
 *
 * @author Mehran Behnam
 */
@DataJpaTest
@DirtiesContext
@Testcontainers
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = NONE)
class UserIT {

  /**
   * Spins up a MySQL instance before all tests and tears it down after it.
   */
  @Container
  private static final MySQLContainer MYSQL_CONTAINER = TestContainers.mysqlContainer();

  /**
   * Providers data access operations over {@link User} entity.
   */
  @Autowired
  private UserRepository userRepository;

  @BeforeEach
  void cleanDatabaseAfterEach() {
    userRepository.deleteAll();
  }

  @AfterAll
  static void stopMysql() {
    MYSQL_CONTAINER.stop();
  }

  @Test
  @DisplayName("We should be able persist in database as expected.")
  void Save_WeShouldBeAblePersistInDatabaseAsExpected() {
    User user = new User();
    user.setEmail("example@host.com");
    user.setToken("token");
    user.setRefreshToken("refresh token");
    user.setVerificationCode("123-321");

    userRepository.save(user);

    Optional<User> retrieved = userRepository.findById(user.getId());

    assertThat(retrieved.isPresent()).isTrue();
    assertThat(retrieved.get().getId()).isEqualTo(1L);
    assertThat(retrieved.get().getEmail()).isEqualTo("example@host.com");
    assertThat(retrieved.get().getToken()).isEqualTo("token");
    assertThat(retrieved.get().getRefreshToken()).isEqualTo("refresh token");
    assertThat(retrieved.get().getVerificationCode()).isEqualTo("123-321");

  }

  /******************************************************************************************************************
   * Auxiliary Stuff
   *****************************************************************************************************************/

  @TestConfiguration
  @ComponentScan(basePackageClasses = {
      UserRepository.class
  })
  static class UserITConfig {

  }
}