package ir.mehranbehnam.todoit.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import ir.mehranbehnam.todoit.service.mail.VerificationEmail;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("mail-test")
@AutoConfigureTestDatabase(replace = Replace.NONE)
class SimpleMailServiceIT {

  private static GreenMail smtpServer;

  @Autowired
  private SimpleMailService simpleMailService;

  @BeforeAll
  static void setUp() {
    smtpServer = new GreenMail(new ServerSetup(2525, "localhost", "smtp"));
    smtpServer.start();
  }

  @AfterAll
  static void tearDown() {
    smtpServer.stop();
  }

  @Test
  void shouldSendMail() throws Exception {
    String recipient = "test@test.com";
    String subject = "Verification Mail Test";
    String code = "123-456";

    Map<String, Object> data = new HashMap<>();
    data.put("code", code);

    VerificationEmail email = new VerificationEmail(recipient, subject, data);

    simpleMailService.send(email);

    assertReceivedMessageContains(code);
  }

  private void assertReceivedMessageContains(String expected)
      throws IOException, MessagingException {
    MimeMessage[] receivedMessages = smtpServer.getReceivedMessages();

    assertEquals(1, receivedMessages.length);
    String content = (String) receivedMessages[0].getContent();
    assertTrue(content.contains(expected));
  }

  /******************************************************************************************************************
   * Auxiliary Stuff
   *****************************************************************************************************************/

  @TestConfiguration
  @ComponentScan(basePackageClasses = {
      SimpleMailService.class
  })
  static class GmailServiceITConfig {

  }
}